﻿using GroupAssgment.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GroupAssgment.Controllers
{
    public class HomeController : Controller
    {
        GADbContext db;
        public HomeController()
        {
            db = new GADbContext();
        }
        public ActionResult Index()
        {
            return View(db.Areas.ToList());

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}