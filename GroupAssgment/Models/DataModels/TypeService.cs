﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class TypeService
    {
        [Key]
        public int ID { get; set; }
        public string ServiceName { get; set; }
        public int Status { get; set; }
        public float ServicePrice { get; set; }
        ICollection<OrderDetail> OrderDetails { get; set; }
    }
}