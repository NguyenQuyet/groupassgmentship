﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class OrderDetail
    {
        [Key]
        public int ID { get; set; }
        public string ProductName { get; set; }
        public float ProPrice { get; set; }
        public float Weight { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public Order Order { get; set; }
        public int TypeServiceId { get; set; }
        [ForeignKey("TypeServiceId")]
        public TypeService TypeService { get; set; }
    }
}