﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class Citys
    {
        [Key]
        public int ID { get; set; }
        public string CityName { get; set; }
        public int Status { get; set; }
        public int AreaId { get; set; }
        [ForeignKey("AreaId")]
        public Areas Areas { get; set; }
        ICollection<Store> Stores { get; set; }
    }
}