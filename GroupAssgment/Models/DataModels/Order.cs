﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class Order
    {
        [Key]
        public int ID { get; set; }
        public string OrderCode { get; set; }
        public string SernderName { get; set; }
        public string SendAddress { get; set; }
        public string SenderNumberPhone { get; set; }
        public string RecipientName { get; set; }
        public string RecipientAddress { get; set; }
        public string RecipientNumberPhone { get; set; }
        public float TotalPrice { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public float ShippingCost { get; set; }
        public DateTime Created { get; set; }

        public int StoreId { get; set; }
        [ForeignKey("StoreId")]
        public Store Store { get; set; }
        ICollection<OrderDetail> OrderDetails { get; set; }
    }
}