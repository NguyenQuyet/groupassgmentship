﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class Store
    {
        [Key]
        public int ID { get; set; }
        public string StoreName { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public int CityId { get; set; }
        [ForeignKey("CityId")]
        public Citys Citys { get; set; }
        ICollection<Account> Accounts { get; set; }
        ICollection<Order> Orders { get; set; }
    }
}