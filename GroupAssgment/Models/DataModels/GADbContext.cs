﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GroupAssgment.Models.DataModels
{
    public class GADbContext:DbContext
    {
        public GADbContext():base("name=GroupAssgment"){ }

        public virtual DbSet<Areas> Areas { get; set; }
        public virtual DbSet<Citys> Citys { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<TypeService> TypeServices { get; set; }
        public virtual DbSet<AdminAccount> AdminAccounts { get; set; }

    }
}